# Créer un magasin

![tip](warning "Cette page provient de l'Ancien Wiki. Elle n'est peut-être pas à jour.")

## Créer un magasin d'objets

Vous avez deux possibilités pour créer un magasin d'objets, soit en utilisant la commande d'évènement, soit en utilisant une commande de script :
```ruby
$game_temp.shop_calling = true
$game_temp.shop_goods = [
  [0, id_item1],
  [0, id_item2],
  # etc...
]
```

![tip](info "Il est recommandé d'insérer une commande d'attente après l'appel de script pour que l'interpréteur puisse se synchroniser.")

## Créer un magasin de Pokémon

Pour créer un magasin de Pokémon, utilisez la commande pokemon_shop_open. Cette commande prend trois paramètres, un tableau d'ID de Pokémon, un tableau de prix des Pokémon et un tableau de niveau des Pokémon. Le niveau d'un Pokémon peut être remplacé par un tableau associatif décrivant le Pokémon à générer.

Exemple : Magasin vendant un Pikachu niveau 50 (2500$) et un Miaouss d'Alola niveau 15 (500$)
```ruby
pokemon_shop_open([25, 52], [2500, 500], [50, { id: 52, level: 15, form: 1 }])
```