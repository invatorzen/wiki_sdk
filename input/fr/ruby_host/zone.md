# Editeur de Zone

![tip](warning "Cette page provient de l'Ancien Wiki. Elle n'est peut-être pas à jour.")

## Interface des Zones de RubyHost
Le bouton `Zones` accessible sur l'interface d'accueil de RubyHost permet d'accéder à une fenêtre permettant de modifier les données des différentes maps de votre projet.
![Editeur Zone|center](img/ruby_host/Rubyhostmapsmain.png "Interface d'édition des données des maps")

À savoir :
- Créer une zone et y attribuer un panneau (qui s'affiche à l'entrée de map) spécifique.
- Interdire ou autoriser la Téléportation et le Vol.
- Régler la position du héros quand il se téléporte sur la map et sa position sur la mappemonde.
- Régler la météo.
- Créer les groupes de Pokémon sauvages qui seront présents sur la map (WildEditor, disponible depuis la fenêtre principale de RubyHost).

## Créer une nouvelle zone et la paramétrer
Pour créer une nouvelle zone et la paramétrer avec l'interface prévue à cet effet, il faut suivre certaines étapes :

1. Cliquez sur le `+` à côté de `Zone :`.  
    ![Illustration](img/ruby_host/Plus.png)
2. Rentrez la map qui concerne cette Zone en rentrant son identifiant `Map ID :`.
3. Rentrez le numéro du panneau qui s'affichera à l'entrée de map dans `n° du panneau affiché :`.
4. Suivant s'il s'agit d'une map d'extérieur, d'intérieur, d'une grotte, cochez les cases `Téléportation interdite` et/ou `Vol autorisé`.
    Par exemple : Pour une map d'*extérieur*, **j'autorise le Vol et la téléportation**. Je cocherai donc la deuxième case mais pas la première.
5. Rentrez les coordonnées des positions de téléportation (ou atterrissage si le joueur arrive après avoir lancé Vol) dans `Position X :` et `Position Y :`.
6. Rentrez les coordonnées de la position du héros sur la mappemonde.
7. Cliquez sur `Sauvegarder` pour enregistrer vos modifications.
8. Pour changer le nom de la zone, allez dans l'onglet 10 des Textes :  
    ![Edition de nom de zone](img/ruby_host/Test.png)
9. Vous pouvez tester votre map et vous assurer que les paramètres ont bien été pris en compte.

## Créer un groupe de Pokémon sauvages
Revenez sur la fenêtre principale de RubyHost. Un bouton `Groupes` est disponible et permet d'ouvrir la fenêtre d'édition des groupes de Pokémon sauvages sur la map en question.
![Editeur de Groupe|center](img/ruby_host/Rubyhost_mapswildeditor.png "Interface d'édition des groupes de Pokémon sauvages")

Pour créer un nouveau groupe de Pokémon sauvages :
1. Cliquez sur `Ajouter` dans `Groupes`.
2. Réglez le SystemTag, le Tag, le type de combat, l'écart de niveau et si besoin l'Interrupteur qui active l'apparition de ce groupe dans `Infos du groupe`.
3. Sélectionnez le Pokémon que vous voulez attribuer au groupe (sous `Pokémon`) et cliquez sur `Ajouter`.
4. Sélectionnez le Pokémon dans la liste `Pokémon`.
5. Paramétrez les données du Pokémon.
6. Sauvegardez
