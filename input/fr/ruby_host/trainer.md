# Editeur de Dresseurs

![tip](warning "Cette page provient de l'Ancien Wiki. Elle n'est peut-être pas à jour.")

## Interface des Dresseurs de RubyHost
Le bouton `Dresseurs` accessible sur l'interface d'accueil de RubyHost permet d'ouvrir une fenêtre ayant pour but la création et l'édition des équipes des dresseurs rencontrés en jeu.
![Interface|center](img/ruby_host/Dresseur.png "Interface d'édition des dresseurs")

## Créer un nouveau dresseur
Pour créer un nouveau dresseur, appuyez sur le bouton `Ajouter` sous la colonne `Dresseurs`. Vous pouvez ensuite définir :

### Le nom du dresseur

Vous pouvez définir le nom du dresseur dans la case `Noms`, s'il s'agit d'un combat double, séparez les noms par une virgule (exemple : `Tana, Estelle`).

![tip](info "Si vous voulez changer la classe du dresseur (Scout, Gamin, ...), allez à l'id de votre dresseur dans l'onglet `29 - classes de dresseurs` dans [l'éditeur de Textes](text.md)")

### Son argent

Dans la case `argent de base`. L'argent donné sera l'`argent de base` multiplié au niveau du dernier Pokémon du dresseur.

### Ses phrases

En appuyant sur `Modifier phrase de victoire/défaite`, vous accédez à une page `Text` avec plusieurs lignes de saisies. Choisissez l'ID texte correspondant à l'ID de votre dresseur.

### Le mode combat double

En cochant la case `Combat double`.
![tip](warning "Si vous ne mettez pas deux noms séparés par une virgule dans le champ, vous vous exposez à un bug")

### Son battler

En double-cliquant sur le battler (ou l'espace vide s'il n'y a pas encore de battler)
![tip](info "Il faut que la battler provienne du projet ouvert avec Ruby Host sinon c'est ignoré")
![tip](info "Si jamais vous avez selectionné un Battler du projet ouvert mais qu'il ne se valide pas comme battler. Vérifiez que vous n'avez pas ouvert le projet avec un chemin relatif. Reouvrez votre projet en cliquant sur `Ouvrir` dans la fenêtre principale au cas où.")

### Son groupe spécial

Le groupe spécial est un nombre supérieur à 5 qui permet entre autres de faire des combats scénarisés. Si vous n'en avez pas besoin mettez `0`, si vous en avez besoin mettez l'id du dresseur dans la BDD.

### Ses Pokémons

Vous pouvez ajouter ou supprimer des Pokémons grâce aux boutons sous la colonne équipe puis de les paramétrer. Vous pouvez donc définir son niveau, son bonheur, ses attaques, ses stats IV et EV, etc ...

### Cas particuliers

Le `N° équipe` correspond à l'équipe à laquelle appartient le Pokémon (par défaut 0 dans un combat solo); si vous avez un combat double, mettez 1 pour la seconde équipe (exemple : Si nos deux dresseurs sont Tana et Estelle, Tana possèdera les Pokémons de l'équipe 0 et Estelle les Pokémons de l'équipe 1)

La forme correspond aux différentes formes qu'on peut trouver, cela correspond aux 28 Zarbi + les mégas-évolutions. La forme normale est la forme 0.

Une fois ceci fait, appuyez sur `Sauver` et retournez dans RMXP pour régler l'[évènement de votre Dresseur](fr/event_making/trainer_event.md).
