# Build Under Linux

If you're a Linux User, especially a Debian Like one. We suggest you to manually compile PSDK.

Here's how to do:

## Install the necessary tools
To install and compile the PSDK tools, you’ll need to run the following commands.
Note : If you have issue with « sudo gem install rake rake-compiler » try without sudo.
```
cd
sudo apt-get install git
git clone https://gitlab.com/NuriYuri/PSDK-DEP.git
git clone https://github.com/NuriYuri/Ruby-Fmod.git
git clone https://github.com/NuriYuri/LiteRGSS

sudo apt-get install -y build-essential
sudo apt-get install -y ruby ruby-dev
sudo gem install rake rake-compiler

ruby -v
# ruby -v should show a Ruby version that is more or equal than Ruby 2.5.0
# If this is not, follow the tutorial on this page : https://www.hugeserver.com/kb/install-ruby-rails-debian-ubuntu/
# But instead of setting Ruby 2.4 you set Ruby 2.5.x 

sudo dpkg -i PSDK-DEP/Fmod-for-PSDK.deb
sudo dpkg -i PSDK-DEP/SFML-2.5.deb

cd Ruby-Fmod
rake clean
rake compile
cd ..

cd LiteRGSS
rake clean
sudo rake compile
cd ..
```

## Create a PSDK Project
To create a PSDK project you need to download the 7z PSDK file ending with “Linux” and extract it in a world writable directory using `p7zip` package.

Warning, this is not enough !
You now need to copy the file `~/LiteRGSS/lib/LiteRGSS.so` and `~/Ruby-Fmod/lib/RubyFmod.so` in the project Root (where Game.rb can be found).

## Start PSDK without terminal
Someone created a launcher : [Game](https://www.mediafire.com/file/g6480yd31e4bgby/Game/file)

## Update PSDK
Use this tool : [Updater](https://www.mediafire.com/file/0g77n4lul4n272r/Update/file)

