# Manage quests

![tip](warning "This page is not about creating quests in Ruby Host!")

In PSDK you manage quests through `$quests`. All the methods are explained in the documentation : [PFM::Quests](https://psdk.pokemonworkshop.fr/yard/PFM/Quests.html).

We'll see how to use this variable

## First step start quests

To start a quest, call `$quests.start(id)` where `id` is the id of the quest in Ruby Host.

Once `$quests.start(id)` was called you need to tell the System to check started quests:
```ruby
$quests.check_up_signal
```

This method checks the quests to start and finish. It also shows the quest name in the screen.

![Quest informer|center](https://i.imgur.com/X6OI8Zo.png "Starting a new quest")

## 2nd step : Validate the goal

There's various methods to validate goals
- `$quests.beat_npc(quest_id, npc_name_index)` where `quest_id` is the quest id and `npc_name_index` is the index of the name of the NPC to beat in the goal order of Ruby Host
- `$quests.speak_to_npc(quest_id, npc_name_index)` where the parameters are the same

The other objectives are automatically validated by PSDK.

![tip](warning "You should call `$quests.check_up_signal` at the end of the event that validates the goals.")

## 2nd step bis : Show an hidden goal

To show an hidden goal call:
```ruby
$quests.show_goal(quest_id, goal_index)
```

The parameters are : 
- `quest_id` the quest id.
- `goal_index` index of the goal in the goal order.

![tip](info "You should only hide the Speak to NPC goals!")

## Third step : Check if the quest is finished and give the earning.

To finish a quest you have two thing to do, check if all the goal are done & distribute the earning.

![tip](warning "If you don't distribute the earning, the quest book will not consider the quest as finished.")

### Check if all the goals are done

To check if all the goals are done, write the following line in a condition :
```ruby
$quests.finished?(quest_id)
```

### Check if the earnings were distributed

To check if the earnings were distributed write this line in a condition :
```ruby
$quests.earnings_got?(quest_id)
```

To distribute them use the following command :
```ruby
$quests.get_earnings(quest_id)
```

![tip](info "It's recommanded to check if the quest is finished before checking if the earnings were distributed.")
