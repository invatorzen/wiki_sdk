# Give a badge

![tip](warning "This page comes from the **old Wiki**. It's probably not up to date.")

## Give a badge to the player
### Command

To give a badge to the player you can use the following command :
```ruby
$trainer.set_badge(badge_num, region_num, obtenu_ou_non)
```

Where :
- `badge_num` is the no. of the badge (1, 2, 3, 4, 5, 6, 7, 8)
- `region_num` is the no. of the game region (1, 2, ...). It's 1 by default.
- `obtenu_ou_non` : `true` true if you want the badge to be considered obtained, false otherwise. (true by default).

### Examples

Give the 5th badge :
```ruby
$trainer.set_badge(5)
```

Withdraw the 5th badge :
```ruby
$trainer.set_badge(5, 1, false)
```

Give the 5th badge of the 3rd region :
```ruby
$trainer.set_badge(5, 3)
```

## Getting how many badges the player has got so far

To get exactly how many badges the player possesses, use the following command :
```ruby
$trainer.badge_counter
```

## Check if the player has a specific badge
### Command

To detect if the player has got a specific badge, you can use the command :
```ruby
$trainer.badge_obtained?(badge_num, region_num)
```

### Examples

Check if the player has got the 5th badge:
```ruby
$trainer.badge_obtained?(5)
```

Detect if the player has got the 5th badge of the 3rd region :
```ruby
$trainer.badge_obtained?(5, 3)
```