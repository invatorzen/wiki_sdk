# Some coding rules

In PSDK there's some coding rule to follow. We use `rubocop` but with special settings (because some stuff are anoying or doesn't fit the PSDK use). You can find the Rubocop configuration here : [.rubocop.yml](https://gitlab.com/NuriYuri/pokemonsdk/blob/development/scripts/.rubocop.yml)

![tip](info "There's a old time where `rubocop` wasn't used at all, we didn't try to make the script from that time comply on the `rubocop` rules. We usually update the script coding aspect when we need to modify it.")

## The iterations

In PSDK we strongly discourage anyone to use `for` loops. Ruby `for` loops are syntaxic sugar used to write `.each` call without writing it explicitely. Insead of for you should use :
- `number.times` if you need to iterate from 0 to `number - 1`.
- `number.upto(final)` if you need to iterate from `number` to `final` (included, +1 increment).
- `number.downto(final)` if you need to iterate from `number` to `final` (included, -1 decrement).
- `number.step(step_size, final)` if you need to iterate from `number` to `final` with a step of step_size (`final` is not always the last iteration value).
- `collection.each` if you need to iterate through each item of the collection.
- `array.each_with_index` if you need to iterate through array elements and have their index.
- `array.index` if you need to find the index of an object (matchin criteria) in an Array.
- `enumerable.find` if you need to find a specific value (maching criteria) in an Array or an Enumerator.
- `enumerable.any?` if you need to know if the array/enumerable contain something matching the criteria.  
    ![tip](info "Not giving a block is a way to detect if the array is not empty!")
- `enumerable.all?` if you need to know if the array/enumerable elements all match the criteria.
- `enumerable.none?` if you need to know if none of the array/enumerable elements match the criteria.
- `enumerable.select` if you need to select the element that match the criteria.
- `array.select!` if you want to select the element that match the criteria and make the array contain only those elements.
- `enumerable.reject` if you need to select the element that doesn't match the criteria.
- `array.reject!` if you want to select the element that doesn't match the criteria and make the array contain only those elements.
- `enumerable.collect` or `enumerable.map` if you need to transform an enumerable into an array where the value are transformed by the block.  
    Example :
    ```ruby
    ten_to_nineteen = 10.times.collect { |i| i + 10 }
    # => [10, 11, 12, 13, 14, 15, 16, 17, 18, 19]
    hex_digits = 10.upto(15).collect { |i| i.to_s(16).upcase }
    # => ["A", "B", "C", "D", "E", "F"]
    ```
- `array.collect!` or `array.map!` to transform all the element of the array using the block.

Here's an example of using iterations instead of `for` loops :
```ruby
def find_height(img)
  first_y = img.height.times.find do |y|
    img.width.times.any? { |x| img.get_pixel_alpha(x, y) != 0 }
  end
  return img.height - first_y.to_i
end
```
Here's the for version :
```ruby
def find_height(img)
  first_y = nil
  for y in 0...img.height
    for x in 0...img.width
      break first_y = y if img.get_pixel_alpha(x, y) != 0
    end
    break if first_y
  end
  return img.height - first_y.to_i
end
```

## The nil values

In Ruby the instance variables, global variables contain `nil` if they're not assigned. This often cause issues like `NoMethodError (undefined method 'meth' for nil:NilClass)` to prevent that there's various way that can be used.

### Default value asignment

To assign a default value to a variable (when it's not supposed to be `nil` and we're not in `#initialize`) you have two legit solution :
```ruby
@var = value if @var.nil?
```
or
```ruby
@var = value unless @var
```

The first way is sub-optimal if the variable isn't supposed to be a boolean (ruby will have to call `nil?` before setting the value). The second way is better but not the recommanded way according to `rubocop`.

If you need to un-nil a variable use the `||=` operator :
```ruby
@var ||= value
```

![tip](info "You can use `unless @var`, it's actually **faster** than ||= but you should expect `rubocop` to raise an error.")

### Safe navigation

The safe navigation is something that was introduced in Ruby 2.3. It allows to call methods or chain call methods without adding a bunch of condition thanks to the `&.` keyword.

Here's some example :
```ruby
@system.update if @system
```
can be written like :
```ruby
@system&.update
```

```ruby
if @system && @system.things && @system.things.other
```
can be written like :
```ruby
if @system&.things&.other
```

![tip](info "The `&.` keyword is special because it only check if the value on left is `nil`. If it's nil, the `&.` chain is broken and the result value is `nil` which doesn't validate the condition.")
![tip](warning "The `&.` chaining is not suitable for comparison since it can return `nil` unless you know what you're doing, use the safe navigation for method calling. Note that in Ruby `expr.> value` is valid syntax thus `expr&.> value won't cause an error if expr is nil but it's ugly.")

## Array creation and operation

If you come from the Essentials you probably use the `push` method from an empty array to start filling information (because of the `for` thing or just because you saw that in other scripts).

In PSDK we'll rarely use the `push` method. We prefer using `<<` instead (it works for both array and strings) and in Ruby 2.5 it has its own `opcode` (opt_ltlt).

For the array creation we prefer pre-filling the array instead of pushing each value one-by-one.

Example : 
```ruby
ary = [first_thing, second_thing, third_thing]
```
Instead of :
```ruby
ary = []
ary.push first_thing
ary.push second_thing
ary.push third_thing
```
In Ruby the two methods produce the same result but aren't executed the same way. (The first one use specific `opcode` that are a bit faster than calling `push`).

For array that contain complex values like the list of Pokémon Info Box (in the party for example) we'll use `Array.new(size) { |index| thing_to_push }`. This way of creating an array allows us to execute code before adding each value. 
Example :
```ruby
party_boxes = Array.new(6) { |index| create_info_box(index) }
```
Instead of :
```ruby
party_boxes = []
for index in 0...6
  party_boxes.push create_info_box(index)
end
```
![tip](info "For multi-line blocks, we'll tend to use `next(value)` to explicitely say what's stored.")

![tip](info "Sometimes there's no other solution than using `<<` to add value to an array (especially when the conditions that gives the value existence in the array are complexe).")

## Explicitness

As you may have seen in *Array creation and operation* we'll prefer using explicit code. This is why in the newly produced code you'll see a bunch of method like `create_things` instead of dosen of line of codes in the blocks.

When you can, try to factorize your code in smaller methods. This allow various thing : We know what the code is supposed to do thanks to the method name. We can customize the things using monkey patch/specialization without having to rewrite everything.

In the `rubocop` rule used by PSDK we disabled the rules about implicit `return`/`next`. If a method or a block should return a value to the thing that called it and use more than one line, put the `return`/`next` keyword (according to if it's a method or a block). It helps a lot to read the code.